:set shiftwidth=4
:set tabstop=4
:set expandtab
:set showtabline=2
:set splitbelow
:set splitright
:set ignorecase!
:set path=$PWD/**
:set laststatus=2
:set history=1000
:set autoread
:set incsearch
:set background=dark
:set nobackup
:set nowb
:set noswapfile
:set ai "auto indent
:set wrap "wrap lines

:colorscheme desert

:noremap <C-J> <C-W><C-J>
:noremap <C-K> <C-W><C-K>
:noremap <C-L> <C-W><C-L>
:noremap <C-H> <C-W><C-H>

:nmap ,t <Esc>:tabnew
:imap ,t <Esc>:tabnew
:nmap ,n <Esc>:tabnew<CR>
:imap ,n <Esc>:tabnew<CR>

" Add the current file's directory to the path if not already present.
autocmd BufRead *
      \ let s:tempPath=escape(escape(expand("%:p:h"), ' '), '\ ') |
      \ exec "set path+=".s:tempPath
