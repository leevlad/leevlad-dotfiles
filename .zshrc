###################################
### ZSH SETUP
###################################
export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="miloshadzic"
plugins=(git zsh-syntax-highlighting zsh-history-substring-search)
source $ZSH/oh-my-zsh.sh

###################################
### ALIASES
###################################
# misc
alias zshrc="source ~/.zshrc"
alias zzz="source ~/.zshrc"
alias bex="bundle exec"
alias md="make dev"
alias md2="make dev2"
alias gi="goop install"

# SSH
alias bmo="ssh -A vlad@bmo-ec2-us-east-1a.500px.net"
alias app="ssh -A root@app1.500px.net"
alias api="ssh -A root@api1.500px.net"
alias resizer="ssh -A vlad@resize1-ec2-us-east-1a.500px.net"
alias search="ssh -A vlad@search1-ovh-bhs.500px.net"
alias admin1="ssh -A root@admin1.500px.net"
alias admin2="ssh -A root@admin2.500px.net"
alias stage="ssh -A root@stage1.500px.net"
alias stagec="ssh -A root@stage1.500px.net -t 'screen -dr vlad'"
alias mdb7="ssh -A vlad@mdb7-ovh-bhs.500px.net"
alias px6="ssh -A vlad@pre-500px6.500px.net"
alias px7="ssh -A vlad@pre-500px7.500px.net"
alias px8="ssh -A vlad@pre-500px8.500px.net"
alias etl3="ssh -A vlad@etl3-ovh-bhs.500px.net"
alias etl4="ssh -A vlad@etl4-ovh-bhs.500px.net"
alias kopf="open 'http://localhost:19200/_plugin/kopf/' && ssh esm1-ovh-bhs.500px.net -NL 19200:localhost:9200"
alias luigi="open http://localhost:18085/static/visualiser/index.html && ssh -L 18085:localhost:8085 etl4-ovh-bhs.500px.net"
alias couchbase_console="open 'http://localhost:8091' && chch -w && knife vault show couchbase-prd creds && ssh -L 8091:10.1.1.242:8091 bastion-ovh-bhs.500px.net"
alias redash="ssh -A vlad@redash1-ec2-us-east-1a.500px.net"
alias backup="ssh -A root@backup1.500px.net"
alias nagios="ssh -A vlad@nagios-ec2-us-east-1a.500px.net"
alias report5="ssh -A vlad@report5-ovh-bhs.500px.net"
alias lb1="ssh -A root@lb1.500px.net"
alias lb2="ssh -A root@lb2.500px.net"

# git-related
alias ggpull='git pull --rebase origin $(current_branch)'
alias gst="git status --short"
alias glog="git log --pretty=format:\"%C(yellow)%h %Cgreen%an%Creset %Cred%ar:%Creset %\"\"s\""
alias glogg="git log --pretty=format:\"%C(yellow)%h %Cgreen%an%Creset %Cred%ar:%Creset %\"\"s\" --graph"
alias glog2="git log --graph --stat --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n'' %C(bold yellow)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
alias gcam="git add . && git commit -a"
alias gco="git checkout"
alias gam="git add . && git commit --amend"
alias gbr='git branch -a'
alias grhh='git reset HEAD --hard'
alias gsu="git submodule update"
alias gs="git show"

# navigation
alias dev="cd ~/Developer/500px"
alias px="cd ~/Developer/500px/500px"
alias gou="cd ~/Developer/500px/go-utils"
alias ind="cd ~/Developer/500px/indexer"
alias media="cd ~/Developer/500px/media-service"
alias conv="cd ~/Developer/500px/converter-service"
alias notif="cd ~/Developer/500px/notifications-service"
alias resize="cd ~/Developer/500px/resize-service"
alias ss="cd ~/Developer/500px/search-service"
alias wbff="cd ~/Developer/500px/web-bff-service"
alias mbff="cd ~/Developer/500px/mobile-bff-service"
alias gosrc="cd ~/.go/src/github.com/500px"
alias prime="cd ~/Developer/500px/prime"
alias t="iStats"
alias ws="cd ~/Developer/500px/witchshark"
alias cr="cd ~/Developer/coinramp/coinramp"

# editor
alias s="subl"
alias ss="subl ."
alias sss="subl ~/Developer/"
alias vim="nvim"

# rails related
alias fs="foreman start"
alias zz="zeus start"
alias zs="zeus server"
alias zc="zeus console"
alias zt="zeus test"
alias start-postgres="launchctl load /usr/local/opt/postgresql/homebrew.mxcl.postgresql.plist"
alias stop-postgres="launchctl unload /usr/local/opt/postgresql/homebrew.mxcl.postgresql.plist"

# python related
alias venv="source .venv/bin/activate"

# aws related
# Tails AWS logs. First get the log streams:
#   $ awslogs groups
# then tail it like so:
#   $ tail-aws /aws/lambda/notifications-indexer
alias tail-aws="awslogs get --watch"

alias chef-ruby='eval "$(chef shell-init zsh)"'

###################################
### PATH
###################################
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/X11R6/bin
export PATH=$PATH:/sbin:/usr/sbin
export PATH=$PATH:/Users/vladimirli/bin
export PATH=$PATH:/usr/local/sbin
export PATH=$PATH:$HOME/.rvm/bin
export PATH=$PATH:/opt/chefdk/bin
export PATH=$PATH:/Library/TeX/Distributions/.DefaultTeX/Contents/Programs/texbin
export PATH=$PATH:/System/Library/Frameworks/JavaScriptCore.framework/Versions/Current/Resources
export PATH=$PATH:/Users/vladimirli/Developer/selenium-drivers

###################################
### HELPER FUNCTIONS FOR DEV
###################################
function docker-rebuild() {
    CWD=$(pwd)

    cd ~/Developer/platform

    docker-compose down

    # stop all containers
    docker stop $(docker ps -a -q)

    # kill running containers
    docker kill $(docker ps -q)

    # delete all containers
    docker rm $(docker ps -a -q)

    # delete all images
    docker rmi $(docker images -q)

    # delete dangling volumes
    docker volume rm $(docker volume ls -q -f dangling=true)

    # delete dangling images
    docker rmi $(docker images -q -f dangling=true)

    docker-compose pull
    docker-compose up -d

    cd $CWD
}

function docker-start() {
    CWD=$(pwd)
    cd ~/Developer/platform
    docker-compose up -d

    docker-compose start rmq
    docker-compose start percona
    docker-compose start mongo
    docker-compose start couchbase
    docker-compose start moxi
    docker-compose start elasticsearch
    docker-compose start media
    docker-compose start converter
    docker-compose start resize-varnish
    docker-compose start resize
    docker-compose start search
    docker-compose start indexer
    docker-compose start localqueue
    docker-compose start postgres
    docker-compose start notificationsindexer
    docker-compose start notificationsservice
    docker-compose start roshibackend
    docker-compose start roshi
    docker-compose start timelinesindexer
    docker-compose start timelinesservice
    docker-compose start visionservice
    docker-compose start keyword
    cd $CWD
}

function docker-stop() {
    CWD=$(pwd)
    cd ~/Developer/platform
    docker-compose stop
    cd $CWD
}

function docker-pause() {
    CWD=$(pwd)
    cd ~/Developer/platform
    docker-compose pause
    cd $CWD
}

function docker-unpause() {
    CWD=$(pwd)
    cd ~/Developer/platform
    docker-compose unpause
    cd $CWD
}

# Local env helper commands
function run-indexer() {
    make -C /Users/vladimirli/Developer/500px/indexer
    /Users/vladimirli/Developer/500px/indexer/indexer 500px.photos 500px.users 500px.collections 500px.blog_posts 500px.groups
}

function run-worker() {
    cd ~/Developer/500px/500px
    sidekiq -q immediate -q default -q high -q stats -q upload -q low -q upload_low -q upload_high
}

function run-sidekiq() {
    cd ~/Developer/500px/500px
    rackup sidekiq.ru
}

function run-website() {
    cd ~/Developer/500px/500px
    rails s thin
}

function run-webbff() {
    cd ~/Developer/500px/web-bff-service/
    bundle exec rackup -p 4567
}

function run-prime() {
    cd ~/Developer/500px/prime
    foreman start -m "mysqld=1,mailcatcher=1,memcached=1,redis=1,sidekiq=0"

}

function run-zeus() {
    docker-env
    cd /Users/vladimirli/Developer/500px/500px
    zeus start
}

function mysql-console() {
    docker-env
    mysql -h $DOCKER_IP -uroot -D 500px
}

# this function will symlink goutils into uServices to be used as a dependency so that we can develop locally and
# see updates without having to copy or redeploy
function link-go() {
    echo "linking go-utils into resize-service..."
    rm -rf /Users/vladimirli/Developer/500px/resize-service/vendor/github.com/500px/go-utils
    ln -s $GOROOT/src/github.com/500px/go-utils /Users/vladimirli/Developer/500px/resize-service/vendor/github.com/500px/

    echo "linking go-utils into vcg-mongodb-forwarder..."
    rm -rf /Users/vladimirli/Developer/500px/vcg-mongodb-forwarder/.vendor/github.com/500px/go-utils
    ln -s $GOROOT/src/github.com/500px/go-utils /Users/vladimirli/Developer/500px/vcg-mongodb-forwarder/.vendor/github.com/500px/
}

# this will remove the symlinks to go-utils from uServices
# and then re-run the goop install so that latest source can be fetched from github
function unlink-go() {
    echo "unlinking go-utils from resize-service..."
    rm -rf /Users/vladimirli/Developer/500px/resize-service/vendor/github.com/500px/go-utils
}

function exif() {
    exiftool -a -u -g1 $1
}

function reindex-all() {
    docker exec -it system_indexer_1 /opt/indexer/indexer -amqp-uri=amqp://rmq.container.local:5672 -db-host=percona.container.local:3306 -db-user=root -db-pass="" -mgo-host=mongo.container.local -es-host=elasticsearch.container.local:9200 -reindex 500px.photos
    docker exec -it system_indexer_1 /opt/indexer/indexer -amqp-uri=amqp://rmq.container.local:5672 -db-host=percona.container.local:3306 -db-user=root -db-pass="" -mgo-host=mongo.container.local -es-host=elasticsearch.container.local:9200 -reindex 500px.users
    docker exec -it system_indexer_1 /opt/indexer/indexer -amqp-uri=amqp://rmq.container.local:5672 -db-host=percona.container.local:3306 -db-user=root -db-pass="" -mgo-host=mongo.container.local -es-host=elasticsearch.container.local:9200 -reindex 500px.collections
    docker exec -it system_indexer_1 /opt/indexer/indexer -amqp-uri=amqp://rmq.container.local:5672 -db-host=percona.container.local:3306 -db-user=root -db-pass="" -mgo-host=mongo.container.local -es-host=elasticsearch.container.local:9200 -reindex 500px.groups
    docker exec -it system_indexer_1 /opt/indexer/indexer -amqp-uri=amqp://rmq.container.local:5672 -db-host=percona.container.local:3306 -db-user=root -db-pass="" -mgo-host=mongo.container.local -es-host=elasticsearch.container.local:9200 -reindex 500px.galleries
}

function redis-console {
    redis-cli -h ${DOCKER_IP}
}

function install-eventmachine {
    gem install eventmachine -v '1.0.5' -- --with-cppflags=-I/usr/local/opt/openssl/include
}

# SWITCH BETWEEN OLD AND NEW CHEF
function chch() {
    case $1 in
        o)
            rm ~/.chef
            ln -s ~/.chef_ovh ~/.chef
            # alias berks="echo WRONG CHEF YOU DUMMY!"
            echo Changed to Old Chef
            ;;
        w)
            rm ~/.chef
            ln -s ~/.chef_ws ~/.chef
            unalias berks
            echo Changed to New Chef
            ;;
    esac
}

function prime-tunnel() {
    screen -dmRR search_service -m ssh root@pre-prime1.500px.net -L9999:search.500px.net:80
}

function use-s3-aws() {
    export AWS_ACCESS_KEY_ID=$S3_AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$S3_AWS_SECRET_ACCESS_KEY
}

function use-vcg-aws() {
    export AWS_ACCESS_KEY_ID=$VCG_AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$VCG_AWS_SECRET_ACCESS_KEY
}

function use-ec2-aws() {
    export AWS_ACCESS_KEY_ID=$EC2_AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$EC2_AWS_SECRET_ACCESS_KEY
}

###################################
### CONSOLE SETUP
###################################
# pure prompt: https://github.com/sindresorhus/pure
# autoload -U promptinit; promptinit
# prompt pure

# other
export EDITOR="vim"
export LS_COLORS=""
# export PROMPT='%{$fg[cyan]%}%1~%{$reset_color%}%{$fg[red]%}|%{$reset_color%}$(git_prompt_info)%{$fg[cyan]%}>%{$reset_color%} '
export PROMPT='%{$fg[blue]%}[%D{%Y-%m-%dT%H:%M:%S}]%{$reset_color%} %{$fg[cyan]%}${PWD/#$HOME/~}%{$reset_color%} $(git_prompt_info)
%{$fg[yellow]%}>%{$reset_color%} '
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt appendhistory autocd extendedglob notify
unsetopt beep
unsetopt nomatch
bindkey -e
zstyle :compinstall filename '/Users/vladimirli/.zshrc'
autoload -Uz compinit
compinit
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

# give me more identities for provisioning
ssh-add -K /Users/vladimirli/.aws/keys/vlad-ec2-keypair_platform.pem > /dev/null 2>&1

# this is a bug in OSX Sierra
ssh-add -A > /dev/null 2>&1

###################################
### ENVIRONMENT VARIABLES FOR DEV
###################################
# docker default ip
export DOCKER_IP=127.0.0.1

export AWS_REGION="us-east-1"

# Microservice environment variables
export AWS_KEY="AKIAJPZADL2SCXOJIIHA"
export AWS_SECRET="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"

# VCG dev account creds
export VCG_AWS_ACCESS_KEY_ID="AKIAJKIUQFNY4E4UHWRA"
export VCG_AWS_SECRET_ACCESS_KEY="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"

# S3 Account creds
export S3_AWS_ACCESS_KEY_ID="AKIAJUE23HS266HBIADQ"
export S3_AWS_SECRET_ACCESS_KEY="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"

# EC2 Account creds
export EC2_AWS_ACCESS_KEY_ID="AKIAJ245Z3Z4UELDG5KQ"
export EC2_AWS_SECRET_ACCESS_KEY="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"

# Use one of the above AWS accounts
export AWS_ACCESS_KEY_ID=$EC2_AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$EC2_AWS_SECRET_ACCESS_KEY

# for vcg-mysql-forwarder
export AWS_ACCESS_KEY=$VCG_AWS_ACCESS_KEY_ID
export AWS_SECRET_KEY=$VCG_AWS_SECRET_ACCESS_KEY

# for vcg-activity-forwarder
#export DDB_ACCESS_KEY=$VCG_AWS_ACCESS_KEY_ID
#export DDB_SECRET_KEY=$VCG_AWS_SECRET_ACCESS_KEY

# for keyword-service
export DDB_ACCESS_KEY=$EC2_AWS_ACCESS_KEY_ID
export DDB_SECRET_KEY=$EC2_AWS_SECRET_ACCESS_KEY


# stuff for static sites
export PX_SS_AWS_ACCESS_KEY_ID="AKIAJUE23HS266HBIADQ"
export PX_SS_AWS_SECRET_ACCESS_KEY="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"
export FASTLY_API_KEY="c36118c759a875e9be3c9aa83218085e"

export CHEF_WS_HOME="~/.chef"
export CHEF_WS_USERNAME="vlad"
export MACHINE_ENV="localhost"
export PX_API_HOST="500px.dev:3000"
export PX_API_URL="http://500px.dev:3000/"
export PX_ELASTICSEARCH_HOST="${DOCKER_IP}"
export PX_KINESIS_DSN="redis://${DOCKER_IP}:6379/"
export PX_MEDIA_SERVICE_URL="http://media.500px.dev:8083"
export PX_MEMCACHED_HOST="${DOCKER_IP}:11211"
#export PX_NOTIFICATIONS_DSN="postgres://notifications@${DOCKER_IP}:5432/database?statement_timeout=5000&sslmode=disable&user=notifications&password=notpw"
export PX_NOTIFICATIONS_DSN="postgres://notifications:notpw@${DOCKER_IP}:5432?sslmode=disable"

export PX_MONOLITH_MONGO_HOST="${DOCKER_IP}"
export PX_MONOLITH_MYSQL_HOST="${DOCKER_IP}"

export PX_NOTIFICATIONS_SERVICE_URL="http://${DOCKER_IP}:8096"
export NOTIFICATIONS_SECRET_KEY="devsecret"
export PX_REDIS_HOST="${DOCKER_IP}:6379"
export PX_SEARCH_AMQP_HOST="${DOCKER_IP}"
export PX_SEARCH_SERVICE_URL="http://search.500px.dev:8080"
export PX_TIMELINES_SECRET_KEY="devsecret"
export PX_TIMELINES_SERVICE_URL="http://${DOCKER_IP}:8085"
export PX_TIMELINES_URL="abc"
export PX_VISION_URL="abc"
export S3_AVATAR_SALT="S3_AVATAR_SECRET_VALUE_162ae6dddcd0ffe9f5514a460c51c415"
export S3_PHOTO_SALT="2p934gh9p3hg"

# RESIZER
# export PX_RESIZE_SERVICE_URL="http://500px.dev:9000"
export PX_RESIZE_SERVICE_URL="http://${DOCKER_IP}:9000"
export PX_RESIZER_API_SECRET="zXh9MjQPjsA4DMdx3WvAdFa0bMwScpiqFcReITvD"
export PX_RESIZER_AWS_DDB_ACCESS_KEY="AKIAJ245Z3Z4UELDG5KQ"
export PX_RESIZER_AWS_DDB_SECRET_KEY="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"
export PX_RESIZER_SIGNING_SECRET="794aa9b7b1a5f8d01832d69452f4bf4a"

# Ansible-related stuff
export ANSIBLE_VAULT_SECRET_FILE="/Users/vladimirli/Developer/500px/ansible-playbooks/instance/vault_secret"
export ANSIBLE_SSH_USER=vlad

# Packagecloud token for cleaning script:
# https://github.com/500px/sys/blob/master/script/clean_packagecloud.rb
# this is a personal token for account '500px', found here: https://packagecloud.io/api_token
export PACKAGECLOUD_API_TOKEN="1553d37fa6bea228158280fcfc7bc76eef5d7ff8ea00cb04"

# GO related
export GOROOT=/usr/local/opt/go@1.7/libexec
export GOPATH=$HOME/.go
export PATH=$PATH:$GOPATH/bin

# required for vips and resize-service
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/opt/libxml2/lib/pkgconfig

# VCG-related env vars
export PX_VCG_API_CONSUMER_KEY="dAC1F31FOLDfH40AgWhJKfGrOQF0rnZnybXlqKUW"
export PX_VCG_API_CONSUMER_SECRET="55527fea30c91d9bb925e5ecb382dc12fa2f0529"

# For Sprint Burndown Charts
export ASANA_ACCESS_TOKEN="0/0739250c279e4884f62dd49ba0c05b0d"


#################
#### COINRAMP
#################
# Twilio
export TWILIO_ACCOUNT_SID="AC07a5fb289f9806ef25e951ca5b7e121a"
export TWILIO_AUTH_TOKEN="f176c248bf4e84a6df4dd21956077a52"
export TWILIO_PHONE_NUMBER="6479304464"

# Quadriga
export QUADRIGA_API_KEY="UMZEjlULJh"
export QUADRIGA_API_SECRET="0efdfd861956760ba73f17cefd6ea99c"
export QUADRIGA_CLIENT_ID="2360229"

###################################
### NUMPAD SUPPORT
###################################
# 0 . Enter
bindkey -s "^[Op" "0"
bindkey -s "^[Ol" "."
bindkey -s "^[OM" "^M"
# 1 2 3
bindkey -s "^[Oq" "1"
bindkey -s "^[Or" "2"
bindkey -s "^[Os" "3"
# 4 5 6
bindkey -s "^[Ot" "4"
bindkey -s "^[Ou" "5"
bindkey -s "^[Ov" "6"
# 7 8 9
bindkey -s "^[Ow" "7"
bindkey -s "^[Ox" "8"
bindkey -s "^[Oy" "9"
# + -  * /
bindkey -s "^[Ok" "+"
bindkey -s "^[Om" "-"
bindkey -s "^[Oj" "*"
bindkey -s "^[Oo" "/"

###################################
### ZSH HISTORY SUBSTRING SEARCH SUPPORT
### Allows substring search with arrow keys
### From: https://github.com/zsh-users/zsh-history-substring-search
###################################
# bind UP and DOWN arrow keys
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down

# bind UP and DOWN arrow keys (compatibility fallback
# for Ubuntu 12.04, Fedora 21, and MacOSX 10.9 users)
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# bind P and N for EMACS mode
bindkey -M emacs '^P' history-substring-search-up
bindkey -M emacs '^N' history-substring-search-down

# bind k and j for VI mode
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

# added by travis gem
[ -f /Users/vladimirli/.travis/travis.sh ] && source /Users/vladimirli/.travis/travis.sh

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"
